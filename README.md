# Step Detector Library
This is an android library for step detection. It is a Kotlin library and requires min SDK version 26. If your project supports that then follow the given steps to add this module to your project. 

Download the aar file here : [![](https://i.imgur.com/djDXFTJ.png)](https://github.com/Matt110110/stepdetectorlib)

## How to add aar to your project

1. Go to file > New > New Module...
![new module](https://i.imgur.com/S08GbGF.png)
---

2. Scroll down to Import .JAR/.AAR Package from the new module window
![Import .JAR/.AAR Package](https://i.imgur.com/MHdy56z.png)
---

3. Now you can see stepdetectorlib-release appear in your Project window
![project window](https://i.imgur.com/TUO8Ajq.png)
---

4. One final step, add the following code to your app level `build.gradle` file:
```
implementaton project(':stepdetectorlib-release')
```
![app level build.gradle](https://i.imgur.com/0g611Kf.png)

---
And you are done. Now you can use the module for step detection in your app. If you want a demo implementation, use the steps below.

1. Open android studio
![Android studio](https://i.imgur.com/ludcnFE.png)
---
2. Select the empty activity and click next
![Empty activity](https://i.imgur.com/8FUpehb.png)
---
3. Set the language to Kotlin, and minimum API level to 26. Then click finish.
![Kotlin](https://i.imgur.com/g8SPAn4.png) 

4. Open the main-activity.xml file and add the following code:
```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:gravity="center"
    android:layout_width="match_parent"
    android:layout_height="match_parent">



    <TextView
        android:id="@+id/tv_steps"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:gravity="center"
        />


    <Button
        android:id="@+id/btn_start"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Start Pedometer"
        />

    <Button
        android:id="@+id/btn_stop"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Stop Pedometer"
        />

</LinearLayout>
```
---
5. Open MainActivity.kt file and add the following code.
```kotlin
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.matt.stepdetectiorlib.StepDetector
import com.matt.stepdetectiorlib.StepListener


class MainActivity : AppCompatActivity(),
    SensorEventListener, StepListener {
    private var TvSteps: TextView? = null
    private var simpleStepDetector: StepDetector? = null
    private var sensorManager: SensorManager? = null
    private var accel: Sensor? = null
    private var numSteps = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Get an instance of the SensorManager
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accel = sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        simpleStepDetector = StepDetector()
        simpleStepDetector!!.registerListener(this)
        TvSteps = findViewById<View>(R.id.tv_steps) as TextView
        val BtnStart =
            findViewById<View>(R.id.btn_start) as Button
        val BtnStop =
            findViewById<View>(R.id.btn_stop) as Button
        BtnStart.setOnClickListener {
            numSteps = 0
            sensorManager!!.registerListener(
                this@MainActivity,
                accel,
                SensorManager.SENSOR_DELAY_FASTEST
            )
        }
        BtnStop.setOnClickListener { sensorManager!!.unregisterListener(this@MainActivity) }
    }

    override fun onAccuracyChanged(
        sensor: Sensor,
        accuracy: Int
    ) {
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            simpleStepDetector!!.updateAccel(
                event.timestamp,
                floatArrayOf(event.values[0], event.values[1], event.values[2])
            )
        }
    }

    override fun step(timeNs: Long) {
        numSteps++
        TvSteps!!.text = TEXT_NUM_STEPS + numSteps
    }

    companion object {
        private const val TEXT_NUM_STEPS = "Number of Steps: "
    }
}
```
---
Run the app and it should work perfectly.